import {applyMiddleware, createStore} from "redux";
import cashReducer from "../reducer/cashReducer";
import middlewareExample from "../reducer/middlewareExample";

const middlewareEnhancer = applyMiddleware(middlewareExample)

export const store = createStore(cashReducer, middlewareEnhancer);
