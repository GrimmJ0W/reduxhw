import {useDispatch, useSelector} from "react-redux";
import {ADD_CASH, GET_CASH} from "./reducer/cashReducer";

function App() {
    const dispatch = useDispatch();
    const cash = useSelector(state => state.cash);

    const addCash = (input) =>{
        dispatch({type: ADD_CASH, payload: input})
    }

    const getCash = (input) =>{
        dispatch({type: GET_CASH, payload: input})
    }

    return (
        <>
            <div style={{textAlign: "center"}}>
                <h2>Open console to see middleware effect</h2>
            </div>
            <div style={{
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                height: "600px",
                fontSize: "30px"
            }}>
                {cash + "$"}
                <button onClick={()=>addCash(Number(prompt()))}>Add some cash</button>
                <button onClick={() =>getCash(Number(prompt()))}>Get some cash</button>
            </div>
        </>
    );
}

export default App;
