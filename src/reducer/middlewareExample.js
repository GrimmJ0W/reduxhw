const middlewareExample = (store) => (next) => (action)=>{
    console.log("Transaction type:" + action.type)
    console.log("$ amount per transaction:" + action.payload);
    next(action);
}

export default middlewareExample;
